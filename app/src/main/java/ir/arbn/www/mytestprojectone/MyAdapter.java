package ir.arbn.www.mytestprojectone;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by A.R.B.N on 3/8/2018.
 */

public class MyAdapter extends BaseAdapter {
    Context mContext;
    String myText[];

    public MyAdapter(Context mContext, String[] text) {
        this.mContext = mContext;
        myText = text;
    }

    @Override
    public int getCount() {
        return myText.length;
    }

    @Override
    public Object getItem(int i) {
        return myText[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = LayoutInflater.from(mContext).inflate(R.layout.list_adapter, null);
        TextView textOne = row.findViewById(R.id.textOne);
        textOne.setText(myText[i]);
        return row;
    }
}
