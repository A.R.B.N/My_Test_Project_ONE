package ir.arbn.www.mytestprojectone;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {
    ListView myList;
    Context mContext = HomeActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        myList = findViewById(R.id.myList);
        String textOne[] = {
                "My Test Text ONE",
                "My Test Text TWO",
                "My Test Text THREE",
                "My Test Text FOUR",
                "My Test Text FIVE",
        };
        MyAdapter adapter = new MyAdapter(mContext, textOne);
        myList.setAdapter(adapter);
        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String clickedName = (String) adapterView.getItemAtPosition(i);
                Toast.makeText(mContext, clickedName, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
