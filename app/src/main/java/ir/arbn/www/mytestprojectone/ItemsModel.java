package ir.arbn.www.mytestprojectone;

/**
 * Created by A.R.B.N on 3/10/2018.
 */

public class Items {
    String itemOne;
    String itemTwo;

    public Items(String itemOne, String itemTwo) {
        this.itemOne = itemOne;
        this.itemTwo = itemTwo;
    }

    public String getItemOne() {
        return itemOne;
    }

    public void setItemOne(String itemOne) {
        this.itemOne = itemOne;
    }

    public String getItemTwo() {
        return itemTwo;
    }

    public void setItemTwo(String itemTwo) {
        this.itemTwo = itemTwo;
    }
}
